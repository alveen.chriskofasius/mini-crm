<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Update Company') }}
        </h2>
    </x-slot>

    <head>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    </head>
<body>
    <div class="col-lg-8  display:flex">
        <form method="post" action="{{ url('company', $data ) }}" enctype="multipart/form-data"> 
        @method('put')        
        @csrf
                <div class="mb-3">
                <input class="form-control @error('name') is-invalid
                @enderror" id="name" name="name"  type="text" placeholder="{{__('Company Name')}}" autofocus value="{{old('name',$data->name)}}">
                @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
    
                <div class="mb-3">
                <input type="email" class="form-control @error('email') is-invalid
                @enderror" name="email" id="email" placeholder="{{__('Email')}}" value="{{old('email',$data->email)}}" >
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
                <div class="mb-3">
                <input class="form-control  @error('website') is-invalid
                @enderror" id="website" type="text" name="website" placeholder="{{__('Website')}}" value="{{old('website',$data->website)}}" >
                @error('website')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>

                <div class="mb-3">
                <label for="formFile" class="form-label ">{{__('Select Logo (optional)')}}</label>
                <input type="hidden" name="oldImage" value="{{$data->logo}}">
                    @if ($data->logo)
                        <img src="{{ asset('storage/' .$data->logo)}}" class="img-preview img-fluid mb-3 col-sm-3">
                    @else
                        <img class="img-preview img-fluid mb-3 col-sm-3">
                    @endif
                <input class="form-control @error('logo') is-invalid
                @enderror" type="file" id="logo" name="logo" onchange="previewImage()">
                @error('logo')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>

            </div>
            <div >
                <a type="button" href="/company" class="btn btn-secondary" >{{__('Cancel')}}</a>
                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
            </div>
        </form>  
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
       function previewImage(){
            const image = document.querySelector('#logo');
        const imgPreview = document.querySelector('.img-preview');
        imgPreview.style.display = 'block';
        const oFReader = new FileReader();
        oFReader.readAsDataURL(image.files[0]);
        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
</body>
</x-app-layout>
