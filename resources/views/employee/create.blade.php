<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Employee') }}
        </h2>
    </x-slot>

    <head>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    </head>
<body>
    <div class="col-lg-8  display:flex">
        <form method="post" action="/employee">
                @csrf
                <div class="mb-3">
                <input class="form-control @error('first_name') is-invalid
                @enderror" id="first_name" name="first_name" type="text" placeholder="{{__('First Name')}}" autofocus value="{{old('first_name')}}">
                @error('first_name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
    
                <div class="mb-3">
                <input class="form-control @error('last_name') is-invalid
                @enderror" id="last_name" name="last_name" type="text" placeholder="{{__('Last Name')}}" autofocus value="{{old('last_name')}}">
                @error('last_name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>

                <div class="mb-3">
                <input type="email" class="form-control @error('email') is-invalid
                @enderror" name="email" id="email" placeholder="{{__('Email')}}" value="{{old('email')}}">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>

                <div class="mb-3">
                <input class="form-control  @error('phone') is-invalid
                @enderror" id="phone" type="text" name="phone" placeholder="{{__('Phone')}}" value="{{old('phone')}}">
                @error('phone')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>

                <div class="mb-3">
                <label for="category" class="form-label">{{__('Select Company')}}</label>
                <select class="form-select @error('companyID') is-invalid
                @enderror" name="companyID"> 
                    @foreach ($companies as $company)
                        @if (old('companyID') == $company->id)     
                            <option value="{{ $company->id}}" selected>{{ $company->name}} </option>
                        @else
                            <option value="{{ $company->id}}">{{ $company->name}} </option>
                        @endif
                    @endforeach
                </select>
                @error('companyID')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div >
                <a type="button" href="/employee" class="btn btn-secondary" >{{__('Cancel')}}</a>
                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
            </div>
        </form>  
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</x-app-layout>
