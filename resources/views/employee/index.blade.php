<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Employee') }}
        </h2>
    </x-slot>

    <head>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
    @if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
      
        <a type="button" href="/employee/create" class="btn float-right btn-secondary mt-2 mb-2 ml-2 ">
        {{__('Add Employee')}}
        </a>
     

<table id="table_employee" class="display">
    <thead>
        <tr>
            <th scope="col">{{__('Company Name')}}</th>
            <th scope="col">{{__('First Name')}}</th>
            <th scope="col">{{__('Last Name')}}</th>
            <th scope="col">{{__('Email')}}</th>
            <th scope="col">{{__('Phone')}}</th>
            <th scope="col">{{__('Action')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($list as $employee)
            <tr>
                <td scope="row">{{ $employee->company_name }}</td>
                <td scope="row">{{ $employee->first_name }}</td>
                <td scope="row">{{ $employee->last_name }}</td>
                <td scope="row">{{ $employee->email }}</td>
                <td scope="row">{{ $employee->phone }}</td>
                <td scope="row"> 
                <form action="{{ url('employee', $employee->id ) }}" method="post" class="d-inline">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger" onclick="return confirm('{{__('Are you sure?')}}')">{{__('Delete')}}</button>
                </form>
                <a href="/employee/{{$employee->id}}/edit" type="button" class="btn btn-success">{{__('Update')}}</a>
            </td>
            </tr>
        @endforeach   
      
    </tbody>
</table>
    <script>
    $(document).ready(function(){
        $('#table_employee').DataTable();
    });
</script>
</table>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</x-app-layout>
