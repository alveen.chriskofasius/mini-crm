<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Company;

class CompanyUpdateTest extends TestCase
{
    /** @test */
    public function companyUpdateTest()
    {
        $data = Company::findOrFail(1);
        $data = [
            'name' => 'CompanyExampleTest',
            'email' => 'Example@updatetest.com',
            'website' => 'website example test updated'
        ];
       Company::find(1)->update($data); 
      
        $result = Company::find(1);
        self::assertEquals($data['name'],$result['name']);
        self::assertEquals($data['email'],$result['email']);
        self::assertEquals($data['website'],$result['website']);
    }
}
