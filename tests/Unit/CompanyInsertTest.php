<?php

namespace Tests\Unit;

use App\Models\Company;
use Tests\TestCase;
use App\Http\Controllers\CompanyController;

class CompanyInsertTest extends TestCase
{
   /** @test */
    public function companyInsertTest()
    {
        $newCompany = array(
            'name' => 'CompanyTest',
            'email' => 'Example@example.com',
            'website' => 'website example'
        );
        $data = Company::create($newCompany);
        $result = Company::find($data->id);
        self::assertEquals($data['name'],$result['name']);
        self::assertEquals($data['email'],$result['email']);
        self::assertEquals($data['website'],$result['website']);
    }

}
