<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Employee;

class EmployeeUpdateTest extends TestCase
{
      /** @test */
      public function employeeUpdateTest()
      {
          $data = Employee::findOrFail(1);
          $data = [
              'first_name' => 'Mutia',
              'last_name' => 'Afsana',
              'email' => 'mutiaafsana@gmail.com',
              'phone' => '081244577845'
          ];

          Employee::find(1)->update($data); 

          $result = Employee::find(1);

          self::assertEquals($data['first_name'],$result['first_name']);
          self::assertEquals($data['last_name'],$result['last_name']);
          self::assertEquals($data['email'],$result['email']);
          self::assertEquals($data['phone'],$result['phone']);

      }
}
