<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Employee;

class EmployeeInsertTest extends TestCase
{
    /** @test */
    public function employeeInsertTest()
    {
        $newEmployee = array(
            'first_name' => 'Afifah',
            'last_name' => 'Afshin',
            'email' => 'Example@example.com',
            'phone' => '081285874031',
            'companyID' => '1'
        );
      
        $data = Employee::create($newEmployee);
        $result = Employee::find($data->id);
        self::assertEquals($data['first_name'],$result['first_name']);
        self::assertEquals($data['last_name'],$result['last_name']);
        self::assertEquals($data['email'],$result['email']);
        self::assertEquals($data['phone'],$result['phone']);
        self::assertEquals($data['companyID'],$result['companyID']);

    }
}
