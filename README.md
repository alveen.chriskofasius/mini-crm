## Instructions
1. Clone git https://gitlab.com/alveen.chriskofasius/mini-crm.git
2. Open with VS Code or other IDE you like to use
3. Open XAMPP and start Mysql Service, then create database minicrm (default database)
3. Open terminal in VS Code, then type :
 a. composer install
 b. php artisan key:generate
 c. php artisan storage:link
 d. type php artisan migrate
4. Start server by type php artisan serve
5. Enter url http://127.0.0.1:8000 or just copy and paste in your browser


## Mail Service
1. Go to https://mailtrap.io/
2. Register if you don't have account or Login if you have existing account
3. Go to My inbox
4. There is drop down below "Integrations", Select Laravel 7+ and copy config.
5. Go to .env and paste to replace config below comment "# Replace your mailtrap config here to use mailtrap service"
6. Create company and fill email field and wait for loading, try again if there is error timeout or not responding

Note :
There is still a bug in lang when change language in UI create or update company and employee and I am not able to solve it right now.
The email is simple template and text, to notified that you are registered in the website.
