<?php

namespace App\Http\Controllers;
use App\Models\Employee;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = DB::table('employees')
        ->join('companies', 'employees.companyID', '=', 'companies.id')
        ->select('companies.name as company_name', 'employees.*')
        ->get();
    	return view('employee.index', ['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.create',[
            'companies' => Company::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $locale = App::currentLocale();
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name'=> 'required',
            'email'=> 'required|unique:employees',
            'phone' => 'required|digits_between:10,13|unique:employees',
            'companyID' => 'required'
        ]);
   

        Employee::create($validatedData);
       return redirect('employee')->with('success',$locale == 'id' ? 'Data berhasil disimpan' : 'Data saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::findOrFail($id);
        return view('employee.edit', ['data' => $data, 'companies' => Company::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $locale = App::currentLocale();
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name'=> 'required',
            'email' => ['required', Rule::unique('employees', 'email')->ignore($id, 'id')],
            'phone' => ['required','digits_between:10,13', Rule::unique('employees', 'phone')->ignore($id, 'id')],
            'companyID' => 'required'
        ]);
    
          Employee::find($id)->update($validatedData); 
          return redirect('employee')->with('success',$locale == 'id' ? 'Data berhasil diperbaharui' : 'Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $locale = App::currentLocale();
        Employee::destroy($id);
        return redirect('employee')->with('success',$locale == 'id' ? 'Data berhasil dihapus' : 'Data has been deleted');
    }
}
