<?php

namespace App\Http\Controllers;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class CompanyController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $list = Company::all();

    	return view('company.index', ['list' => $list]);

    }
    public function create(){
        return view('company.create');
    }

    public function store(Request $request)
    {
          $locale = App::currentLocale();
          $validatedData = $request->validate([
              'name' => 'required|unique:companies',
              'email'=> 'required|unique:companies',
              'website' => 'required|unique:companies',
              'logo' => 'image|file|dimensions:min_width=100,min_height=100'
          ]);
  
          if($request->file('logo')){
            $validatedData['logo']= $request->file('logo')->store('image-logos');
          }

         $user = Company::create($validatedData);
         $this->email($user);
         return redirect('company')->with('success',$locale == 'id' ? 'Data berhasil disimpan' : 'Data saved successfully');
    }

    public function email($user){
        $locale = App::currentLocale();
        if($locale == 'id'){
            \Mail::raw("Hello $user->name, Terima kasih telah mendapaftarkan perusahaan anda",function($message) use ($user){
                $message->to($user->email,$user->name);
                $message->subject('Selamat, anda menjadi salah satu dari kami');
                });
        }
         \Mail::raw("Hello $user->name, Thank you for register your company",function($message) use ($user){
             $message->to($user->email,$user->name);
             $message->subject('Congratulations, you are one of us');
             });
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
       
        $data = Company::findOrFail($id);
         return view('company.edit', ['data' => $data]);
    }

    public function update(Request $request,$id)
    {
        $locale = App::currentLocale();

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => ['required', Rule::unique('companies', 'email')->ignore($id, 'id')],
            'website' => ['required', Rule::unique('companies', 'website')->ignore($id, 'id')],
            'logo' => 'image|file|dimensions:min_width=100,min_height=100'
        ]);
    
         if($request->file('logo')){
             if($request->oldImage){
                 Storage::delete($request->oldImage);
             }
           $validatedData['logo']= $request->file('logo')->store('image-logos');
         } 
         Company::find($id)->update($validatedData); 
         return redirect('company')->with('success',$locale == 'id' ? 'Data berhasil di perbaharui' : ' Data has been updated');
    }

    public function destroy(Company $company)
    {
        $locale = App::currentLocale();

        if($company->logo){
            Storage::delete($company->logo);
        }
        $count = DB::table('employees')->where('companyID',$company->id)->count();
    
        if($count > 0){
            return redirect('company')->with('failed',$locale == 'id' ? 'Gagal hapus data (masih ada karyawan di perusahaan ini)' : 'Failed delete data (there is an employee exists in this company)');
        }else{
            Company::destroy($company->id);
            return redirect('company')->with('success',$locale == 'id' ? 'Data berhasil dihapus' : 'Data has been deleted');
        }
    }
}
